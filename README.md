## #SMBIOS

iMac13,2 Intel Core i5-3470S @ 2.90 GHz

## #ACPI Section

#### -- Add

| File Name             | Table  | Description                                              |
| :-------------------- | :----- | :------------------------------------------------------- |
| `DMAR.aml`            | `DMAR` | Fix Apple VTD                                            |
| `DSDT.aml`            | `DSDT` | Fix Some Error and Pathing to SSDT                       |
| `SSDT-CPUPM.aml`      | `SSDT` | Enable CPU PM Support                                    |
| `SSDT-DSMFix.aml`     | `SSDT` | Fix Various Problem Around DSM                           |
| `SSDT-EC-DESKTOP.aml` | `SSDT` | Disable EC for Desktop                                   |
| `SSDT-GPRW.aml`       | `SSDT` | Fix GPRW for Instant Wake                                |
| `SSDT-IMEI.aml`       | `SSDT` | Add IMEI Device to ACPI                                  |
| `SSDT-SATA.aml`       | `SSDT` | Fix OEM Table Sata because renaming SAT0 to SATA in DSDT |
| `SSDT-SBUS-MCHC.aml`  | `SSDT` | Fix for SMBUS and MCHC                                   |
| `SSDT-USB.aml`        | `SSDT` | Add Some Property to USB Devices                         |

#### -- Delete

| Comment           | Description       |
| :---------------- | :---------------- |
| `Delete SataTabl` | Drop Oem SataTabl |
| `Delete DMAR`     | Drop OEM DMAR     |

## #Booter Section

#### -- Patch

| Comment               | Description                                      |
| :-------------------- | :----------------------------------------------- |
| `Skip Board ID check` | Allow Any Version of MacOS to boot to Any SMBIOS |

## #Device Properties Section

#### --Add

| PCI                           | Table                 | Value    | Description                                 |
| :---------------------------- | :-------------------- | :------- | :------------------------------------------ |
| `PciRoot(0x0)/Pci(0x16,0x0)`  | `device-id`           | 3A1E0000 | Enable if Using 3th Gen procie on H61 Board |
| `PciRoot(0x0)/Pci(0x1B,0x0))` | `layout-id`           | 1        | Apple ALC layout-id Value for Audio Patch   |
| `PciRoot(0x0)/Pci(0x2,0x0)`   | `AAPL,ig-platform-id` | 06006201 | Connectorless Framebuffer                   |
|                               | `device-id`           | 62010000 | Faking Device ID to Intel HD 2500           |
|                               | `enable-metal`        | 1        | Enable Metal Offline Rendering              |

## #Kernel

#### -- Add Kext

| Kext Name               | Description                                                                                                                          |
| :---------------------- | :----------------------------------------------------------------------------------------------------------------------------------- |
| `Lilu`                  | LILU                                                                                                                                 |
| `VirtualSMC`            | Add Virtual SMC Support                                                                                                              |
| `SMCProcessor`          | Virtual SMC Plugin                                                                                                                   |
| `SMCSuperIO`            | Virtual SMC Plugin                                                                                                                   |
| `WhateverGreen`         | Add Many GPU Support                                                                                                                 |
| `AppleALC`              | Add Audio Support                                                                                                                    |
| `RealtekRTL8111`        | ADD Ethernet Support                                                                                                                 |
| `VoodooPS2Controller`   | Add PS2 Input Support                                                                                                                |
| `BluetoothMcDodo`       | Force Load generic USB Bluetooth on BigSur (Broken in Monterey)                                                                      |
| `USBInjectAll`          | Not Use, Only For Testing                                                                                                            |
| `USBMap-DisableFront`   | USBMap without Front Input for Fix Instant Wake (IDK how to Fix Instant Wake without this) (do not use with **USBMap-DisableFront**) |
| `USBMap-EnableAll`      | USBMap enable All Input (do not use with **USBMap-DisableFront**)                                                                    |
| `BlueToolFixup`         | Fix Bluetooth UserSpace on Monterey+                                                                                                 |
| `FeatureUnlock`         | Add Feature Unlock                                                                                                                   |
| `HoRNDIS`               | For RNDIS Android USB Tethering                                                                                                      |
| `AutoPkgInstaller.kext` | Auto Package Installer for OCLP                                                                                                      |

#### -- Patch

| Comment                                          | Description                                                 |
| :----------------------------------------------- | :---------------------------------------------------------- |
| `Force FileVault on Broken Seal`                 | Enable FileFault on Hackintosh Even after OCLP Root Pathing |
| `Disable Library Validation Enforcement`         | Disable Library Validation Enforcement                      |
| `Reroute kern.hv_vmm_present patch (1)`          | Allow Any Version of MacOS to Install with Any SMBIOS       |
| `Reroute kern.hv_vmm_present patch (2) Legacy`   | Allow Any Version of MacOS to Install with Any SMBIOS       |
| `Reroute kern.hv_vmm_present patch (2) Ventura`  | Allow Any Version of MacOS to Install with Any SMBIOS       |
| `Disable _csr_check() in _vnode_check_signature` | Disable CSR Check                                           |
